function randArray(array) {return array[Math.floor(Math.random()*array.length)];}

export class QuestHook extends Application {
    constructor(...args) {
        super(...args);
    }
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "fgen-questhook";
        options.template = "modules/fgen/templates/questhook.html"
        options.classes.push("fgen-questhook");
        options.resizable = false;
        options.height = "auto";
        options.width = 1000;
        options.minimizable = true;
        options.title = "Quest Hook Generator"
        return options;
    }
    
    async getData(){
        let context = {}
        context.problem = this.problem ??= await this.getCardForType('problem')
        context.antagonist = this.antagonist ??= await this.getCardForType('antagonist')
        context.location = this.location ??= await this.getCardForType('location')
        context.helper = this.helper ??= await this.getCardForType('helper')
        context.solution = this.solution ??= await this.getCardForType('solution')
        return context
    }

    async getCardForType(type) {
        if (type === 'problem'){ return await this.randomCard({types:['Creature','Enchantment']}); }
        if (type === 'antagonist'){ return await this.randomCard({types:['Creature']}); }
        if (type === 'location'){ return await this.randomCard({types:['Land']}); }
        if (type === 'helper'){ return await this.randomCard({types:['Creature']}); }
        if (type === 'solution'){ return await this.randomCard({types:['Artifact','Sorcery','Instant']}); }
        return await this.randomCard();
    }

    async randomCard({types=['Artifact','Creature','Enchantment','Instant','Land','Sorcery'], colors=[]}={}) {
        let cards = []
        for (let type of types) { cards = cards.concat(await QuestHook.getJSON(`modules/fgen/mtg_cards/${type}.json`)); }
        if (colors != []) cards = cards.filter(c => colors.every(this_color => c.colors.includes(this_color)) || c.type_line.includes('Land'))
        return randArray(cards)
    }

    createImageJournal({problem=this.problem,antagonist=this.antagonist,location=this.location,helper=this.helper,solution=this.solution,name="Randomly generated quest hook title"}={}){
        JournalEntry.create({
            name: name,
            flags: {core: {viewMode: JournalSheet.VIEW_MODES.MULTIPLE}},
            pages:[
                {type: "image", name: `Problem - ${problem.name}`, src: problem.art, image:{caption:problem.name}},
                {type: "image", name: `Antagonist - ${antagonist.name}`, src: antagonist.art, image:{caption:antagonist.name}},
                {type: "image", name: `Location - ${location.name}`, src: location.art, image:{caption:location.name}},
                {type: "image", name: `Helper - ${helper.name}`, src: helper.art, image:{caption:helper.name}},
                {type: "image", name: `Solution - ${solution.name}`, src: solution.art, image:{caption:solution.name}}
            ]
        })
    }

    createTextJournal({problem=this.problem,antagonist=this.antagonist,location=this.location,helper=this.helper,solution=this.solution,name="Randomly generated quest hook title"}={}){
        JournalEntry.create({
            name: name,
            pages:[
                {type: "text", name: "Quest hook", text:{content: `<p>Problem - ${problem.name}<br/>Antagonist - ${antagonist.name}<br/>Location - ${location.name}<br/>Helper - ${helper.name}<br/>Solution - ${solution.name}</p>`}}
            ]
        })
    }

    activateListeners(html) {
        super.activateListeners(html)
        
        html.find(".fgen-reroll").click(async ev => {
            let type = ev.target.parentNode.dataset.type
            this[type] = await this.getCardForType(type)
            this.render()
        })

        html.find(".fgen-reroll-all").click(async ev => {
            for (let type of ['problem','antagonist','location','helper','solution']) { this[type] = await this.getCardForType(type) }
            this.render()
        })

        html.find(".fgen-journal-export.image").click(async ev => {
            let data = {}
            let name = html.find('[name=journal-name]').val()
            if (name) data.name = name
            this.createImageJournal(data)
            ui.notifications.info("Saved quest hook to journal")
        })
        html.find(".fgen-journal-export.text").click(async ev => {
            let data = {}
            let name = html.find('[name=journal-name]').val()
            if (name) data.name = name
            this.createTextJournal(data)
            ui.notifications.info("Saved quest hook to journal")
        })

    }

    // Define a cache for JSON data files to reduce loading
    static _cache = {}
    static async getJSON(path) {
        if (QuestHook._cache[path]) return QuestHook._cache[path];
        const data = await foundry.utils.fetchJsonWithTimeout(path)
        QuestHook._cache[path] = data;
        return data;
    }

}

export async function downloadData() {
    ui.notifications.info("Downloading new MTG card data")
    let set_types = ['core','expansion','masters','commander','promo']
    let card_types = ['Artifact','Creature','Enchantment','Instant','Land','Sorcery']

    //var sets = foundry.utils.fetchJsonWithTimeout('https://api.scryfall.com/sets').then(response => response.data.filter(i => set_types.includes(i.set_type)))
    let set_codes = new Set(await foundry.utils.fetchJsonWithTimeout('https://api.scryfall.com/sets').then(response => response.data.filter(i => set_types.includes(i.set_type)).map(i => i.code)))
    let card_url = await foundry.utils.fetchJsonWithTimeout('https://api.scryfall.com/bulk-data').then(response => response.data.find(i => i.type ==='oracle_cards').download_uri)
    // card_url is currently 'https://c2.scryfall.com/file/scryfall-bulk/oracle-cards/oracle-cards-20220910210351.json'
    let cards = await foundry.utils.fetchJsonWithTimeout(card_url)
    cards = cards.filter(i => ! i.type_line.includes('Basic Land'))
        .filter(i => ['normal','leveler'].includes(i.layout))
        .filter(i => 'image_uris' in i)
        .filter(i => set_codes.has(i.set))
        .map(({name,image_uris,layout,frame,colors,type_line}) => ({name,layout,frame,colors,type_line,art: image_uris.normal}))

    for (let type of card_types){
        const newFile = new File([JSON.stringify(cards.filter(i => i.type_line.includes(type)))], `${type}.json`, { type: "application/json" });
        await FilePicker.upload("data", "modules/fgen/mtg_cards/", newFile, {}, {notify:false});
    }
    ui.notifications.info("Finished downloading MTG card data")
}