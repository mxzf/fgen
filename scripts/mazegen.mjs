
export class MazeGen extends Application {
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "fgen-mazegen";
        options.template = "modules/fgen/templates/mazegen.html"
        options.classes.push("fgen-mazegen");
        options.resizable = false;
        options.height = "auto";
        options.width = 400;
        options.minimizable = true;
        options.title = "Maze Generator"
        return options;
    }
    
    activateListeners(html) {
        super.activateListeners(html)
        
        
        html.find(".generate-maze").click(async ev => {
            try {
                let scene_name = html.find('[name="scene_name"]').val()
                let x_size = html.find('[name="x_size"]').val()
                let y_size = html.find('[name="y_size"]').val()
                let grid_size = html.find('[name="grid_size"]').val()
                
                let sparseness = html.find('[name="sparseness"]').val()
                let invisible = html.find('[name="invisible"]')[0].checked
                let image = html.find('[name="image"]').val()
                
                let url = `https://fgen.lffg.org/rest/mazegen` +
                                `?name="${scene_name}"` +
                                `&x_size=${x_size}` +
                                `&y_size=${y_size}` +
                                `&grid_size=${grid_size}` +
                                `&sparseness=${sparseness}` +
                                `&image=${image}`
                
                this.close();
                ui.notifications.notify("Generating maze")
                let maze = await fetch(url).then(response => response.json()).then(data=> Scene.create(data));
                if (invisible) maze.updateEmbeddedDocuments("Wall", maze.walls.map(w => {return {_id:w.id, sight:CONST.WALL_SENSE_TYPES.NONE, light:CONST.WALL_SENSE_TYPES.NONE, sound:CONST.WALL_SENSE_TYPES.NONE}}))
                maze.view()
            }
            catch (e) {
                ui.notifications.error("Error: " + e)
            }
        })
    }       
}