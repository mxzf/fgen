I'm not 100% sure if including this information in the module is *required*, since I'm not actually distributing any copyright material at all, just including some code that can hit Scryfall's API for MtG card info, but links to usage policies never hurt.  

# Scryfall

Scryfall card database downloaded via their API (by individual clients, not distributed in the module)
https://scryfall.com/docs/api

# Magic: The Gathering

Foundry Generator is unofficial Fan Content permitted under the Fan Content Policy. Not approved/endorsed by Wizards. Portions of the materials used are property of Wizards of the Coast. ©Wizards of the Coast LLC.
https://company.wizards.com/en/legal/fancontentpolicy