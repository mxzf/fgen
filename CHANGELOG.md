# Changelog

## 0.2.1 - V11 update

Updated hooks in order to render properly in V11

## 0.2.0 - Public release

Initial public release

### Features
* Random maze scene generation
* Random quest hook generation

## 0.1.0 - Initial testing

Mostly done as an initial experiment into the practicality of generating scenes from JSON responses of generated scenes