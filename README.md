# Foundry Generator
A tool for randomly generating things for Foundry VTT

## Installation

Module manifest: https://gitlab.com/mxzf/fgen/-/releases/permalink/latest/downloads/module.json

## Things this tool can randomly generate
* [Maze scenes](documentation/mazegen.md)
* [Quest hooks](documentation/questhook.md)
* Hopefully more to come 
