# Random quest hook generation

This tool is for generating random quest hooks using Magic the Gathering card names.  It generates a set of Problem, Antagonist, Location, Helper, and Solution prompts that can be used for a quick and easy idea of something for the party to do.  

## Usage

Unless disabled (as a setting) a "Generate Quest Hook" button will be present in the Roll Tables tab of the right sidebar.  Clicking that will open up a dialog box with a set of cards representing a quest hook and buttons for re-rolling all cards or individual cards and also saving the quest to a journal (either as a set of five image pages or as a single text page).

![image](questhook_example.webp)

Each category pulls from its own set of MtG card types to better suit the output to the types of card chosen:
* Problem - Creature, Enchantment
* Antagonist - Creature
* Location - Land (specifically, non-basic lands)
* Helper - Creature
* Solution - Artifact, Sorcery, Instant

## Implementation details

A list of MtG cards is downloaded from Scryfall's API endpoint (specifically, the Oracle Cards dataset).  The card list is filtered to remove cards of various odd-ball sets (such as Un sets) and card formats (such as double-faced cards).  They are also filtered to remove basic lands, cards without associated images available, and so on.  

Cards are then written out to a JSON file according to their type, among these types: Artifact, Creature, Enchantment, Instant, Land, Sorcery.  The data included is also trimmed down to the critical fields used by the module.  These datasets are stored in the user data (it currently comes to ~5MB of data) in the module's folder to be loaded as-needed.  

The first time any given JSON file is loaded, it will be loaded to a cache to be used for subsequent random card selections.  
