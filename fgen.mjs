import {MazeGen} from './scripts/mazegen.mjs'
import {QuestHook, downloadData} from './scripts/questhook.mjs'

// Check for the presence of the 
Hooks.on('ready', async () => {
  let file_list = await FilePicker.browse("data", "modules/fgen/mtg_cards/").then(picker => picker.files)
  if (! ['Artifact','Creature','Enchantment','Instant','Land','Sorcery'].map(type =>`modules/fgen/mtg_cards/${type}.json`).every(filename => file_list.includes(filename))) {
    downloadData()
  }
});


//await fetch(`modules/fgen/mtg_cards/${type}.json`, { method: 'head' }).then(resp => resp.ok)

Hooks.on('init', () => {
  game.settings.register('fgen', 'mazegen', {
    name: "Enable maze generation",
    hint: 'Display the "Maze Generator" button in the Scenes sidebar tab',
    scope: "world",
    config: true,
    requiresReload: true,
    type: Boolean,
    default: true
  });
  game.settings.register('fgen', 'questgen', {
    name: "Enable quest generation",
    hint: 'Display the "Generate Quest Hook" button in the RollTables sidebar tab',
    scope: "world",
    config: true,
    requiresReload: true,
    type: Boolean,
    default: true
  });
})

Hooks.on('renderSceneDirectory', (app,html,context) => {
  if (game.settings.get('fgen','mazegen')) {
    let button = $("<button class='generate-maze'>Maze Generator</button>")
    button.click(() => {new MazeGen().render(true);});
    //html.find(".directory-footer").append(button);
    html.find(".header-actions").append(button);
  }
})
Hooks.on('renderRollTableDirectory', (app,html,context) => {
  if (game.user.isGM && game.settings.get('fgen','questgen')) {
    let button = $("<button class='generate-maze'>Generate Quest Hook</button>")
    button.click(() => {new QuestHook().render(true);});
    html.find(".header-actions").append(button);
  }
})